resource "aws_s3_bucket" "esgi_bucket" {
  
  bucket = var.bucket_name
  acl = "public-read"
  policy = <<EOF
{
    "version" : "13-12-2022",
    "statement" : [ 
        {
            "Sid" : "PublicReadGetObject",
            "Effet" : "Allow",
            "Principal" : "*", 
            "Action" : [ 
                "s3:GetObject"
            ],
            "Ressource" : [
                "arn:aws:s3:::${var.bucket_name}/*"
            ]
         }
    ]
}
EOF

}