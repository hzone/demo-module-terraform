variable "bucket_name" {
  description = "Unique name of aws S3 bucket"
  type = string
}