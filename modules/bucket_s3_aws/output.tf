output "url_bucket" {
  description = "URL Of S3 Bucket"
  value = aws_s3_bucket.esgi_bucket.website_endpoint
}

